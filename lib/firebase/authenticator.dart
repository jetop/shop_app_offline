class Authenticator {

  Future<bool> login() async {
    await Future.delayed(const Duration(milliseconds: 500)); // Waiting for login!
    return true;
  }

}