import 'package:flutter/material.dart';
import '../../fake_data/products_list_file.dart';
import '../../support_classes/product_data.dart';
import '../product_page/product_page.dart';

class ProductList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
        children: ProductsListData.data
            .map((ProductData data) => elementBuild(data))
            .toList());
  }

  Widget elementBuild(ProductData data) {
    return ProductListElement(data);
  }

}

class ProductListElement extends StatelessWidget {
  final ProductData data;

  ProductListElement(this.data);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
        data.name,
        style: TextStyle(
          fontWeight: FontWeight.w500,
          fontSize: 20,
        ),
      ),
      trailing: Text(data.price.toString() + "€"),
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => ProductPage(data)));
      },
    );
  }
}
