import 'package:flutter/material.dart';
import 'product_list.dart';

class ProductListScreen extends StatelessWidget {
  final String title = 'Product list';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(title)),
      bottomNavigationBar: BottomAppBar(
        child: Row(
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.menu),
              onPressed: () => _showModalBottomSheet(context),
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () => {},
        icon: Icon(Icons.camera_alt),
        label: Text('Scan product'),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      body: ProductList(),
    );
  }

  void _showModalBottomSheet(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bottomSheetContext) {
          return Wrap(
            children: <Widget>[
              ListTile(
                title: Center(
                  child: Text(
                    'Manage list',
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 20,
                    ),
                  ),
                ),
              ),
              ListTile(
                  leading: Icon(Icons.add),
                  title: Text('Add product'),
                  onTap: () => Navigator.pop(context)),
              ListTile(
                leading: Icon(Icons.delete),
                title: Text('Delete product'),
                onTap: () => Navigator.pop(context),
              ),
            ],
          );
        });
  }
}
