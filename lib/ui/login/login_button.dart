import 'package:flutter/material.dart';
import '../../firebase/authenticator.dart';
import '../product_list/product_list_screen.dart';

class LoginButton extends StatelessWidget {
  final loginText = 'Login!';
  final Authenticator authenticator = new Authenticator();

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      child: Text(loginText),
      onPressed: () async {
        await authenticator.login();
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) => ProductListScreen()));
      },
    );
  }
}
