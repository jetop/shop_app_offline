import 'package:flutter/material.dart';
import './ui/login/login_screen.dart';

// This runs the app
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // Return the container of the whole application
    return MaterialApp(
      title: 'Shop App',
      theme: ThemeData(
        // Choose whatever color you like!
        primarySwatch: Colors.blue,
      ),
      // This is the root of our app
      home: LoginScreen(),
    );
  }
}
