# Shop App (offline)

A new project to learn Flutter.

## What is this?

Welcome to Shop App, an app thought to learn Flutter's basics. Is is being made by Edoardo and Matteo, and it will be part of the workshop about app development at JUNITEC. In case you have any questions, please feel free to get in touch at [e.debenedetti@jetop.com](mailto:e.debenedetti@jetop.com) or [m.demartis@jetop.com](mailto:m.demartis@jetop.com).

This app is thought to be used just offline, without any kind of backend. The part with the backend (implemented with Firebase), take a look at [this](https://bitbucket.org/jetop/shop_app_online) repository.

The app has some very basic features, such as a (fake) login screen, a list of the available products in the shop, and some info about them.